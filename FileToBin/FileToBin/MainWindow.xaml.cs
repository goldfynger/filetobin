﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace FileToBin
{
    /// <summary></summary>
    public partial class MainWindow : Window
    {
        private Settings _settings = new Settings();
        
        private const string _fileName = "filetobin.settings";
        
        private bool _isInitialized;

        public MainWindow()
        {
            InitializeComponent();

            Load();

            ApplySettings();

            _isInitialized = true;

            Save();
        }

        private void HandleFileOpen(string[] files)
        {
            if (files == null || files.Length == 0) return;

            foreach (var file in files)
            {
                var openedFile = _settings.IsBinary ? (dynamic)new BinaryFile(file, _settings.Binary) : (dynamic)new StringFile(file, _settings.String);

                var lengthStr = new StringBuilder(); ;
                lengthStr.Append(openedFile.Length.ToString());
                while (lengthStr.Length < 15) lengthStr.Append(' ');

                tbList.Text += $"{lengthStr}{openedFile.Path}{Environment.NewLine}";

                tbArrays.Text += openedFile.Result;
            }
        }

        private void TextBlock_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

                HandleFileOpen(files);
            }
        }

        private void bClear_Click(object sender, RoutedEventArgs e)
        {
            tbList.Clear();
            tbArrays.Clear();
        }        

        private void bAdd_Click(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog { Multiselect = true };

            if (openFileDialog.ShowDialog() == true)
            {
                HandleFileOpen(openFileDialog.FileNames);
            }
        }

        private void cbView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _settings.IsBinary = ((ComboBoxItem)cbView.SelectedItem).Content.ToString() == "Binary";

            ApplySettings();

            FillSettings();

            Save();
        }

        private void cbLength_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FillSettings();

            Save();
        }

        private void cbTabWidth_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FillSettings();

            Save();
        }

        private void cbShowLength_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FillSettings();

            Save();
        }


        private void FillSettings()
        {
            if (!_isInitialized) return;


            var length = int.Parse(((ComboBoxItem)cbLength.SelectedItem).Content.ToString());

            if (_settings.IsBinary)
            {
                _settings.Binary.Length = length;
            }
            else
            {
                _settings.String.Length = length;
            }


            var tabs = int.Parse(((ComboBoxItem)cbTabWidth.SelectedItem).Content.ToString());

            if (_settings.IsBinary)
            {
                _settings.Binary.TabsLength = tabs;
            }
            else
            {
                _settings.String.TabsLength = tabs;
            }


            var show = ((ComboBoxItem)cbShowLength.SelectedItem).Content.ToString() == "Show length";

            if (_settings.IsBinary)
            {
                _settings.Binary.ShowLength = show;
            }
            else
            {
                _settings.String.ShowLength = show;
            }
        }


        private void ApplySettings()
        {
            _isInitialized = false;

            cbView.SelectedIndex = _settings.IsBinary ? 0 : 1;

            var subSettings = _settings.IsBinary ? _settings.Binary : _settings.String;


            switch (subSettings.Length)
            {
                case 16: cbLength.SelectedIndex = 0; break;
                case 32: cbLength.SelectedIndex = 1; break;
                case 48: cbLength.SelectedIndex = 2; break;
                case 64: cbLength.SelectedIndex = 3; break;
                case 96: cbLength.SelectedIndex = 4; break;
                case 128: cbLength.SelectedIndex = 5; break;
                case 192: cbLength.SelectedIndex = 6; break;
                case 256: cbLength.SelectedIndex = 7; break;
                default: cbLength.SelectedIndex = 1; break;
            }


            switch (subSettings.TabsLength)
            {
                case 2: cbTabWidth.SelectedIndex = 0; break;
                case 4: cbTabWidth.SelectedIndex = 1; break;
                case 6: cbTabWidth.SelectedIndex = 2; break;
                case 8: cbTabWidth.SelectedIndex = 3; break;
                default: cbTabWidth.SelectedIndex = 1; break;
            }


            cbShowLength.SelectedIndex = subSettings.ShowLength ? 0 : 1;

            _isInitialized = true;
        }


        private void Save()
        {
            if (!_isInitialized) return;

            var serializer = new DataContractJsonSerializer(typeof(Settings));

            using (var stream = new MemoryStream())
            {
                serializer.WriteObject(stream, _settings);

                stream.Position = 0;

                var directory = $"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\{Assembly.GetExecutingAssembly().GetName().Name}";

                if (!Directory.Exists(directory)) Directory.CreateDirectory(directory);

                using (var fileStream = new FileStream($"{directory}\\{_fileName}", FileMode.Create))
                {
                    stream.WriteTo(fileStream);
                }
            }
        }

        private void Load()
        {
            try
            {
                using (var fileStream = new FileStream($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\{Assembly.GetExecutingAssembly().GetName().Name}\\{_fileName}", FileMode.Open))
                {
                    using (var stream = new MemoryStream())
                    {
                        fileStream.CopyTo(stream);

                        stream.Position = 0;

                        var serializer = new DataContractJsonSerializer(typeof(Settings));

                        _settings = (Settings)serializer.ReadObject(stream);
                    }
                }
            }
            catch (Exception)
            {
                _settings = new Settings();
            }
        }
    }

    internal sealed class BinaryFile
    {
        private byte[] _content;
        private string _result;
        private string _path;


        public BinaryFile(string file, SubSettings subSettings)
        {
            _content = File.ReadAllBytes(file);
            _path = file;

            var result = new StringBuilder();

            var fileName = System.IO.Path.GetFileName(file);

            result.Append($"{Environment.NewLine}const uint8_t _{fileName.Split('.')[0]}[{(subSettings.ShowLength ? (_content.Length).ToString() : string.Empty)}] =");
            result.Append($" /* {_content.Length} {fileName} */{Environment.NewLine}{{{Environment.NewLine}");

            var strCount = _content.Length / subSettings.Length;

            if ((_content.Length % subSettings.Length) != 0) strCount++;

            var counter = 0;

            for (var i = 0; i < strCount; i++)
            {
                for (var j = 0; j < subSettings.TabsLength; j++) result.Append(' ');

                for (var j = 0; j < subSettings.Length; j++)
                {
                    if (counter == _content.Length) break;

                    result.Append($"0x{_content[counter++].ToString("X2")}");

                    if (j != subSettings.Length - 1 && counter != _content.Length) result.Append(", ");
                }

                if (i != strCount - 1) result.Append($",{Environment.NewLine}");
            }

            result.Append($"{Environment.NewLine}}};{Environment.NewLine}");

            _result = result.ToString();
        }


        public string Path => _path;

        public string Result => _result;

        public int Length => _content.Length;
    }


    internal sealed class StringFile
    {
        private string _content;
        private string _result;
        private string _path;


        public StringFile(string file, SubSettings subSettings)
        {
            _content = File.ReadAllText(file, Encoding.GetEncoding(1252));
            _path = file;

            var zeroSymbols = false;
            var unexpectedSymbols = false;

            var raw = new StringBuilder(_content);
            var rawCounter = 0;
            while (rawCounter < raw.Length)
            {
                if (raw[rawCounter] == '"') raw.Insert(rawCounter++, '\\');
                else if (raw[rawCounter] == '\r')
                {
                    raw[rawCounter] = 'r';
                    raw.Insert(rawCounter++, '\\');
                }
                else if (raw[rawCounter] == '\n')
                {
                    raw[rawCounter] = 'n';
                    raw.Insert(rawCounter++, '\\');
                }
                else if (raw[rawCounter] == '\t')
                {
                    raw[rawCounter] = 't';
                    raw.Insert(rawCounter++, '\\');
                }
                else if (raw[rawCounter] == '\0')
                {
                    raw[rawCounter] = '0';
                    raw.Insert(rawCounter++, '\\');

                    zeroSymbols = true;
                }
                else if (raw[rawCounter] == '\\')
                {
                    raw[rawCounter] = '\\';
                    raw.Insert(rawCounter++, '\\');
                }
                else if (raw[rawCounter] < 0x20 || raw[rawCounter] == 0x7F)
                {
                    unexpectedSymbols = true;
                }

                rawCounter++;
            }

            var result = new StringBuilder();

            var fileName = System.IO.Path.GetFileName(file);

            var warningStr = !zeroSymbols && !unexpectedSymbols ? string.Empty :
                $"Warning:{(zeroSymbols ? " Zero symbol(s) in file;" : string.Empty)}{(unexpectedSymbols ? " Unexpected character(s) in file;" : string.Empty)}";

            result.Append($"{Environment.NewLine}const char _{fileName.Split('.')[0]}[{(subSettings.ShowLength ? (_content.Length + 1).ToString() : string.Empty)}] =");
            result.Append($" /* {_content.Length}+1 {fileName} {warningStr} */{Environment.NewLine}{{{Environment.NewLine}");

            var rawStr = raw.ToString();

            var counter = 0;

            while (true)
            {
                for (var j = 0; j < subSettings.TabsLength; j++) result.Append(' ');

                var length = (counter + subSettings.Length > rawStr.Length - 1) ? rawStr.Length - counter : subSettings.Length;

                if (rawStr[counter + length - 1] == '\\' && counter + length != rawStr.Length) length++;

                result.Append($"\"{rawStr.Substring(counter, length)}\"");

                counter += length;

                if (rawStr.Length != counter) result.Append($"{Environment.NewLine}");
                else break;
            }

            result.Append($"{Environment.NewLine}}};{Environment.NewLine}");

            _result = result.ToString();
        }


        public string Path => _path;

        public string Result => _result;

        public int Length => _content.Length;
    }


    [DataContract]
    internal sealed class Settings
    {
        [DataMember]
        public bool IsBinary = true;

        [DataMember]
        public SubSettings Binary = new SubSettings();
        
        [DataMember]
        public SubSettings String = new SubSettings();
    }

    [DataContract]
    internal sealed class SubSettings
    {
        [DataMember]
        public int Length = 32;

        [DataMember]
        public int TabsLength = 4;

        [DataMember]
        public bool ShowLength = false;
    }
}