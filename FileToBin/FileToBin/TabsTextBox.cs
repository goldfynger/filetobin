﻿using System;
using System.Windows.Controls;
using System.Windows.Input;

namespace FileToBin
{
    internal sealed class TabsTextBox : TextBox
    {

        private int _tabSize = 4;


        public int TabSize
        {
            get { return _tabSize; }
            set
            {
                if (value <= 0 || value > 8) throw new ArgumentOutOfRangeException(nameof(value));

                _tabSize = value;
            }
        }


        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            if (e.Key == Key.Tab)
            {
                var tab = new string(' ', _tabSize);
                var caretPosition = CaretIndex;
                Text = Text.Insert(caretPosition, tab);
                CaretIndex = caretPosition + TabSize;
                e.Handled = true;
            }
        }
    }
}