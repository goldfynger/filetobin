﻿using System;
using System.Collections.ObjectModel;

using Utils.DataContainers;

namespace FileToBin
{
    /// <summary></summary>
    internal abstract class SettingsTreeNode : NotifyContainer
    {
        /// <summary></summary>
        private string _name;


        /// <summary></summary>
        public string Name
        {
            get { return _name; }
            set { SetField(ref _name, value); }
        }
        

        /// <summary></summary>
        /// <param name="propertyChangedActions"></param>
        protected SettingsTreeNode(ReadOnlyDictionary<string, Action<object>> propertyChangedActions)
        {
            if (propertyChangedActions != null)
            {
                PropertyChanged += (sender, e) =>
                {
                    Action<object> action;

                    if (propertyChangedActions.TryGetValue(e.PropertyName, out action))
                    {
                        action(sender.GetType().GetProperty(e.PropertyName).GetValue(sender));
                    }
                };
            }
        }
    }

    /// <summary></summary>
    internal sealed class SettingsTreeNodeCollection : SettingsTreeNode
    {
        /// <summary></summary>
        private ReadOnlyCollection<SettingsTreeNode> _collection = new ReadOnlyCollection<SettingsTreeNode>(new ObservableCollection<SettingsTreeNode>());

        /// <summary></summary>
        private bool _isExpanded = false;


        /// <summary></summary>
        public ReadOnlyCollection<SettingsTreeNode> Collection
        {
            get { return _collection; }
            set { SetField(ref _collection, value); }
        }

        /// <summary></summary>
        public bool IsExpanded
        {
            get { return _isExpanded; }
            set { SetField(ref _isExpanded, value); }
        }


        /// <summary></summary>
        public SettingsTreeNodeCollection() : base(null) { }

        /// <summary></summary>
        /// <param name="propertyChangedActions"></param>
        public SettingsTreeNodeCollection(ReadOnlyDictionary<string, Action<object>> propertyChangedActions) : base(propertyChangedActions) { }
    }

    /// <summary></summary>
    internal sealed class SettingsTreeNodeComboBox : SettingsTreeNode
    {
        /// <summary></summary>
        private ReadOnlyCollection<string> _collection = new ReadOnlyCollection<string>(new ObservableCollection<string>());

        /// <summary></summary>
        private int _selectedIndex = 0;


        /// <summary></summary>
        public ReadOnlyCollection<string> Collection
        {
            get { return _collection; }
            set { SetField(ref _collection, value); }
        }

        /// <summary></summary>
        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set { SetField(ref _selectedIndex, value); }
        }


        /// <summary></summary>
        public SettingsTreeNodeComboBox() : base(null) { }

        /// <summary></summary>
        /// <param name="propertyChangedActions"></param>
        public SettingsTreeNodeComboBox(ReadOnlyDictionary<string, Action<object>> propertyChangedActions) : base(propertyChangedActions) { }
    }

    /// <summary></summary>
    internal sealed class SettingsTreeNodeCheckBox : SettingsTreeNode
    {
        /// <summary></summary>
        private bool _isChecked = false;


        /// <summary></summary>
        public bool IsChecked
        {
            get { return _isChecked; }
            set { SetField(ref _isChecked, value); }
        }


        /// <summary></summary>
        public SettingsTreeNodeCheckBox() : base(null) { }

        /// <summary></summary>
        /// <param name="propertyChangedActions"></param>
        public SettingsTreeNodeCheckBox(ReadOnlyDictionary<string, Action<object>> propertyChangedActions) : base(propertyChangedActions) { }
    }
}