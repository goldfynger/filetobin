﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using Utils.DataContainers;
using WPFUtils.Commands;

namespace FileToBin
{
    /// <summary></summary>
    internal sealed class FilesTreeNode : NotifyContainer
    {
        /// <summary></summary>
        private readonly string _id;

        /// <summary></summary>
        private string _name;

        /// <summary></summary>
        private string _fullName;

        /// <summary></summary>
        private bool _isBinary;

        /// <summary></summary>
        private bool _isString;

        /// <summary></summary>
        private bool _isSelected;

        /// <summary></summary>
        private bool _showConfirmation;

        /// <summary></summary>
        private Command _confirmCommand = new Command((Action)null);

        /// <summary></summary>
        private Command _cancelCommand = new Command((Action)null);

        /// <summary></summary>
        private FileTypes _fileType = FileTypes.Unknown;

        /// <summary></summary>
        private bool _minify;

        /// <summary></summary>
        private bool _canMinify;

        /// <summary></summary>
        private string _minifyToolTip;

        /// <summary></summary>
        private BinaryMinifierDelegate _binaryMinifier = b => b;

        /// <summary></summary>
        private StringMinifierDelegate _stringMinifier = s => s;

        /// <summary></summary>
        private bool _saveMinified;

        /// <summary></summary>
        private bool _canSaveMinified;

        /// <summary></summary>
        private bool _createArray;


        /// <summary></summary>
        public string Id => _id;

        /// <summary></summary>
        public string Name
        {
            get { return _name; }
            private set { SetField(ref _name, value); }
        }

        /// <summary></summary>
        public string FullName
        {
            get { return _fullName; }
            set { SetField(ref _fullName, value); }
        }

        /// <summary></summary>
        public bool IsBinary
        {
            get { return _isBinary; }
            set { SetField(ref _isBinary, value); }
        }

        /// <summary></summary>
        public bool IsString
        {
            get { return _isString; }
            set { SetField(ref _isString, value); }
        }

        /// <summary></summary>
        public bool IsSelected
        {
            get { return _isSelected; }
            set { SetField(ref _isSelected, value); }
        }

        /// <summary></summary>
        public bool ShowConfirmation
        {
            get { return _showConfirmation; }
            set { SetField(ref _showConfirmation, value); }
        }

        /// <summary></summary>
        public Command ConfirmCommand => _confirmCommand;

        /// <summary></summary>
        public Command CancelCommand => _cancelCommand;

        /// <summary></summary>
        public FileTypes FileType
        {
            get { return _fileType; }
            private set { SetField(ref _fileType, value); }
        }

        /// <summary></summary>
        public bool Minify
        {
            get { return _minify; }
            set { SetField(ref _minify, value); }
        }

        /// <summary></summary>
        public bool CanMinify
        {
            get { return _canMinify; }
            private set { SetField(ref _canMinify, value); }
        }

        /// <summary></summary>
        public string MinifyToolTip
        {
            get { return _minifyToolTip; }
            private set { SetField(ref _minifyToolTip, value); }
        }

        /// <summary></summary>
        public bool SaveMinified
        {
            get { return _saveMinified; }
            set { SetField(ref _saveMinified, value); }
        }

        /// <summary></summary>
        public bool CanSaveMinified
        {
            get { return _canSaveMinified; }
            private set { SetField(ref _canSaveMinified, value); }
        }

        /// <summary></summary>
        public bool CreateArray
        {
            get { return _createArray; }
            set { SetField(ref _createArray, value); }
        }


        /// <summary></summary>
        public event CommandEventHandler RemoveRequested;        


        /// <summary></summary>
        public FilesTreeNode(string id)
        {
            _id = id;

            _confirmCommand.Executed += (sender, e) => RemoveRequested?.Invoke(this, e);
            _cancelCommand.Executed += (sender, e) => ShowConfirmation = false;
        }


        /// <summary></summary>
        /// <param name="propertyName"></param>
        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (propertyName == nameof(FullName))
            {
                var name = Path.GetFileName(FullName);

                if (name.Length > 37)
                {
                    name = $"{name.Substring(0, 20)} . . . {name.Substring(name.Length - 10, 10)}";
                }

                Name = name;

                CheckFileType(); 
            }

            if (propertyName == nameof(FileType))
            {
                CanMinify = _fileType != FileTypes.Unknown;

                Minify = _canMinify;

                MinifyToolTip = _canMinify ? $"Minify file as {_fileType.ToString()}" : null;

                SaveMinified = _canMinify;

                CanSaveMinified = _canMinify;

                CreateArray = true;
            }

            if (propertyName == nameof(Minify))
            {
                CanSaveMinified = _minify;
            }

            base.OnPropertyChanged(propertyName);
        }

        /// <summary></summary>
        /// <returns></returns>
        private void CheckFileType()
        {
            if (XmlMinifier.IsValidXml(_fullName))
            {
                FileType = FileTypes.Xml;

                _stringMinifier = XmlMinifier.MinifyXml;

                _binaryMinifier = b => b;
            }
            else
            {
                FileType = FileTypes.Unknown;

                _stringMinifier = s => s;

                _binaryMinifier = b => b;
            }
        }

        /// <summary></summary>
        /// <returns></returns>
        public byte[] GetBinaryContent() => File.ReadAllBytes(_fullName);

        /// <summary></summary>
        /// <returns></returns>
        public string GetStringContent() => File.ReadAllText(_fullName, Encoding.GetEncoding(1252));

        /// <summary></summary>
        /// <returns></returns>
        public byte[] GetBinaryMinifyContent() => _binaryMinifier(GetBinaryContent());

        /// <summary></summary>
        /// <returns></returns>
        public string GetStringMinifyContent() => _stringMinifier(GetStringContent());


        /// <summary></summary>
        public enum FileTypes
        {
            /// <summary></summary>
            Unknown = 0,
            /// <summary></summary>
            Xml
        }


        /// <summary></summary>
        /// <param name="content"></param>
        /// <returns></returns>
        private delegate byte[] BinaryMinifierDelegate(byte[] content);

        /// <summary></summary>
        /// <param name="content"></param>
        /// <returns></returns>
        private delegate string StringMinifierDelegate(string content);
    }
}