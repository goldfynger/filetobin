﻿using System.IO;
using System.Text;
using System.Xml;

namespace FileToBin
{
    /// <summary></summary>
    internal static class XmlMinifier
    {
        /// <summary></summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static bool IsValidXml(string file)
        {
            var content = File.ReadAllText(file, Encoding.GetEncoding(1252));

            try
            {
                new XmlDocument().LoadXml(content);
            }
            catch
            {
                return false;
            }

            return true;
        }

        /// <summary></summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static string MinifyXml(string xml)
        {
            var document = new XmlDocument();

            var settings = new XmlReaderSettings();
            settings.IgnoreComments = true;
            var reader = XmlReader.Create(new MemoryStream(Encoding.UTF8.GetBytes(xml)), settings);

            document.Load(reader);

            return document.InnerXml;
        }
    }
}