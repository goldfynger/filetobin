﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace FileToBin
{
    /// <summary></summary>
    internal static class SettingsManager
    {
        /// <summary></summary>
        private const string _fileName = "filetobin_v2.settings";

        /// <summary></summary>
        private static bool _saveAllowed = false;


        /// <summary></summary>
        /// <param name="action"></param>
        /// <param name="settings"></param>
        public static void ChangeAndSave(Action<Settings> action, Settings settings)
        {
            action?.Invoke(settings);

            Save(settings);
        }

        /// <summary></summary>
        /// <param name="settings"></param>
        public static void Save(Settings settings)
        {
            if (!_saveAllowed) return;

            var serializer = new DataContractJsonSerializer(typeof(Settings));

            using (var stream = new MemoryStream())
            {
                serializer.WriteObject(stream, settings);

                stream.Position = 0;

                var directory = $"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\{Assembly.GetExecutingAssembly().GetName().Name}";

                if (!Directory.Exists(directory)) Directory.CreateDirectory(directory);

                using (var fileStream = new FileStream($"{directory}\\{_fileName}", FileMode.Create))
                {
                    stream.WriteTo(fileStream);
                }
            }
        }

        /// <summary></summary>
        /// <returns></returns>
        public static Settings Load()
        {
            try
            {
                using (var fileStream = new FileStream($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\{Assembly.GetExecutingAssembly().GetName().Name}\\{_fileName}", FileMode.Open))
                {
                    using (var stream = new MemoryStream())
                    {
                        fileStream.CopyTo(stream);

                        stream.Position = 0;

                        var serializer = new DataContractJsonSerializer(typeof(Settings));

                        var settings = (Settings)serializer.ReadObject(stream);

                        if (settings.BinarySettings == null || settings.StringSettings == null || settings.FileSettings == null) throw new Exception("Invalid settings");

                        return settings;
                    }
                }
            }
            catch (Exception)
            {
                return new Settings();
            }
        }

        /// <summary></summary>
        public static void AllowSave() => _saveAllowed = true;

        /// <summary></summary>
        public static void DisallowSave() => _saveAllowed = false;
    }


    /// <summary></summary>
    [DataContract]
    internal sealed class Settings
    {
        /// <summary></summary>
        [DataMember]
        public bool IsWindowStateMaximized = true;

        /// <summary></summary>
        [DataMember]
        public BinarySettings BinarySettings = new BinarySettings();

        /// <summary></summary>
        [DataMember]
        public StringSettings StringSettings = new StringSettings();

        /// <summary></summary>
        [DataMember]
        public List<FileSettings> FileSettings = new List<FileSettings>();
    }

    /// <summary></summary>
    [DataContract]
    internal sealed class BinarySettings
    {
        /// <summary></summary>
        [DataMember]
        public bool IsExpanded = true;

        /// <summary></summary>
        [DataMember]
        public string[] LengthCollection = new List<int> { 16, 32, 48, 64 }.Select(i => i.ToString()).ToArray();

        /// <summary></summary>
        [DataMember]
        public int LengthIndex = 1;

        /// <summary></summary>
        [DataMember]
        public string[] TabLengthCollection = new List<int> { 2, 4, 6, 8 }.Select(i => i.ToString()).ToArray();

        /// <summary></summary>
        [DataMember]
        public int TabLengthIndex = 1;

        /// <summary></summary>
        [DataMember]
        public bool AddArrayLength = false;

        /// <summary></summary>
        [DataMember]
        public bool AddArrayInfo = false;
    }

    /// <summary></summary>
    [DataContract]
    internal sealed class StringSettings
    {
        /// <summary></summary>
        [DataMember]
        public bool IsExpanded = true;

        /// <summary></summary>
        [DataMember]
        public string[] LengthCollection = new List<int> { 64, 96, 128, 192, 256 }.Select(i => i.ToString()).ToArray();

        /// <summary></summary>
        [DataMember]
        public int LengthIndex = 2;

        /// <summary></summary>
        [DataMember]
        public string[] TabLengthCollection = new List<int> { 2, 4, 6, 8 }.Select(i => i.ToString()).ToArray();

        /// <summary></summary>
        [DataMember]
        public int TabLengthIndex = 1;

        /// <summary></summary>
        [DataMember]
        public bool AddArrayLength = false;

        /// <summary></summary>
        [DataMember]
        public bool AddArrayInfo = false;
    }

    /// <summary></summary>
    [DataContract]
    internal sealed class FileSettings
    {
        /// <summary></summary>
        [DataMember]
        public string FullName = "%empty%";

        /// <summary></summary>
        [DataMember]
        public string Id = Guid.NewGuid().ToString();

        /// <summary></summary>
        [DataMember]
        public bool IsBinary = false;

        /// <summary></summary>
        [DataMember]
        public bool IsString = false;
    }
}