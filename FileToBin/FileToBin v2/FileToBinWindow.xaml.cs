﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Xml;

using Utils.Collections;
using WPFUtils.Commands;

namespace FileToBin
{
    /// <summary></summary>
    internal partial class FileToBinWindow : Window
    {
        /// <summary></summary>
        private readonly Settings _settings = SettingsManager.Load();


        /// <summary></summary>
        private readonly ReadOnlyCollection<SettingsTreeNode> _settingsCollection;

        /// <summary></summary>
        private readonly TypedObservableCollection<FilesTreeNode> _filesCollection;


        /// <summary></summary>
        public ReadOnlyCollection<SettingsTreeNode> SettingsCollection => _settingsCollection;

        /// <summary></summary>
        public TypedObservableCollection<FilesTreeNode> FilesCollection => _filesCollection;


        /// <summary></summary>
        public FileToBinWindow()
        {
            _settingsCollection = CreateSettingsCollection();

            _filesCollection = CreateFilesCollection();


            InitializeComponent();

            WindowState = _settings.IsWindowStateMaximized ? WindowState.Maximized : WindowState.Normal;
            

            SettingsManager.AllowSave();

            SettingsManager.Save(_settings);
        }

        /// <summary></summary>
        /// <param name="e"></param>
        protected override void OnStateChanged(EventArgs e)
        {
            base.OnStateChanged(e);

            SettingsManager.ChangeAndSave(s => s.IsWindowStateMaximized = WindowState == WindowState.Maximized, _settings);
        }

        /// <summary></summary>
        private ReadOnlyCollection<SettingsTreeNode> CreateSettingsCollection()
        {
            return new ReadOnlyCollection<SettingsTreeNode>(new ObservableCollection<SettingsTreeNode>
            {
                new SettingsTreeNodeCollection(new ReadOnlyDictionary<string, Action<object>>(new Dictionary<string, Action<object>>
                {
                    { nameof(SettingsTreeNodeCollection.IsExpanded), b => SettingsManager.ChangeAndSave(s => s.BinarySettings.IsExpanded = (bool)b, _settings) }
                }))
                {
                    Name = "Binary",
                    IsExpanded = _settings.BinarySettings.IsExpanded,
                    Collection = new TypedReadOnlyObservableCollection<SettingsTreeNode>(new ObservableCollection<SettingsTreeNode>
                    {
                        new SettingsTreeNodeComboBox(new ReadOnlyDictionary<string, Action<object>>(new Dictionary<string, Action<object>>
                        {
                            { nameof(SettingsTreeNodeComboBox.SelectedIndex), d => SettingsManager.ChangeAndSave(s => s.BinarySettings.LengthIndex = (int)d, _settings) }
                        }))
                        {
                            Name = "Length",
                            SelectedIndex = _settings.BinarySettings.LengthIndex,
                            Collection = new ReadOnlyCollection<string>(_settings.BinarySettings.LengthCollection)
                        },
                        new SettingsTreeNodeComboBox(new ReadOnlyDictionary<string, Action<object>>(new Dictionary<string, Action<object>>
                        {
                            { nameof(SettingsTreeNodeComboBox.SelectedIndex), d => SettingsManager.ChangeAndSave(s => s.BinarySettings.TabLengthIndex = (int)d, _settings) }
                        }))
                        {
                            Name = "Tab length",
                            SelectedIndex = _settings.BinarySettings.TabLengthIndex,
                            Collection = new ReadOnlyCollection<string>(_settings.BinarySettings.TabLengthCollection)
                        },
                        new SettingsTreeNodeCheckBox(new ReadOnlyDictionary<string, Action<object>>(new Dictionary<string, Action<object>>
                        {
                            { nameof(SettingsTreeNodeCheckBox.IsChecked), b => SettingsManager.ChangeAndSave(s => s.BinarySettings.AddArrayLength = (bool)b, _settings) }
                        }))
                        {
                            Name = "Add array length",
                            IsChecked = _settings.BinarySettings.AddArrayLength
                        },
                        new SettingsTreeNodeCheckBox(new ReadOnlyDictionary<string, Action<object>>(new Dictionary<string, Action<object>>
                        {
                            { nameof(SettingsTreeNodeCheckBox.IsChecked), b => SettingsManager.ChangeAndSave(s => s.BinarySettings.AddArrayInfo = (bool)b, _settings) }
                        }))
                        {
                            Name = "Add array info",
                            IsChecked = _settings.BinarySettings.AddArrayInfo
                        }
                    })
                },
                new SettingsTreeNodeCollection(new ReadOnlyDictionary<string, Action<object>>(new Dictionary<string, Action<object>>
                {
                    { nameof(SettingsTreeNodeCollection.IsExpanded), b => SettingsManager.ChangeAndSave(s => s.StringSettings.IsExpanded = (bool)b, _settings) }
                }))
                {
                    Name = "String",
                    IsExpanded = _settings.StringSettings.IsExpanded,
                    Collection = new TypedReadOnlyObservableCollection<SettingsTreeNode>(new ObservableCollection<SettingsTreeNode>
                    {
                        new SettingsTreeNodeComboBox(new ReadOnlyDictionary<string, Action<object>>(new Dictionary<string, Action<object>>
                        {
                            { nameof(SettingsTreeNodeComboBox.SelectedIndex), d => SettingsManager.ChangeAndSave(s => s.StringSettings.LengthIndex = (int)d, _settings) }
                        }))
                        {
                            Name = "Length",
                            SelectedIndex = _settings.StringSettings.LengthIndex,
                            Collection = new ReadOnlyCollection<string>(_settings.StringSettings.LengthCollection)
                        },
                        new SettingsTreeNodeComboBox(new ReadOnlyDictionary<string, Action<object>>(new Dictionary<string, Action<object>>
                        {
                            { nameof(SettingsTreeNodeComboBox.SelectedIndex), d => SettingsManager.ChangeAndSave(s => s.StringSettings.TabLengthIndex = (int)d, _settings) }
                        }))
                        {
                            Name = "Tab length",
                            SelectedIndex = _settings.StringSettings.TabLengthIndex,
                            Collection = new ReadOnlyCollection<string>(_settings.StringSettings.TabLengthCollection)
                        },
                        new SettingsTreeNodeCheckBox(new ReadOnlyDictionary<string, Action<object>>(new Dictionary<string, Action<object>>
                        {
                            { nameof(SettingsTreeNodeCheckBox.IsChecked), b => SettingsManager.ChangeAndSave(s => s.StringSettings.AddArrayLength = (bool)b, _settings) }
                        }))
                        {
                            Name = "Add array length",
                            IsChecked = _settings.StringSettings.AddArrayLength
                        },
                        new SettingsTreeNodeCheckBox(new ReadOnlyDictionary<string, Action<object>>(new Dictionary<string, Action<object>>
                        {
                            { nameof(SettingsTreeNodeCheckBox.IsChecked), b => SettingsManager.ChangeAndSave(s => s.StringSettings.AddArrayInfo = (bool)b, _settings) }
                        }))
                        {
                            Name = "Add array info",
                            IsChecked = _settings.StringSettings.AddArrayInfo
                        }
                    })
                }
            });
        }

        /// <summary></summary>
        /// <returns></returns>
        private TypedObservableCollection<FilesTreeNode> CreateFilesCollection()
        {
            var collection = new TypedObservableCollection<FilesTreeNode>
                (new ObservableCollection<FilesTreeNode>(_settings.FileSettings.Select(s => new FilesTreeNode(s.Id) { FullName = s.FullName, IsBinary = s.IsBinary, IsString = s.IsString })));


            PropertyChangedEventHandler onPropertyChanged = (sender, e) =>
            {
                var file = (FilesTreeNode)sender;

                var fileSettings = _settings.FileSettings.FirstOrDefault(s => s.Id == file.Id);

                switch (e.PropertyName)
                {
                    case nameof(FilesTreeNode.FullName): fileSettings.FullName = file.FullName; break;

                    case nameof(FilesTreeNode.IsBinary): fileSettings.IsBinary = file.IsBinary; break;

                    case nameof(FilesTreeNode.IsString): fileSettings.IsString = file.IsString; break;
                }

                SettingsManager.Save(_settings);
            };

            CommandEventHandler onRemoveRequested = (sender, e) => _filesCollection.Remove(sender as FilesTreeNode);


            collection.ForEach(f => f.PropertyChanged += onPropertyChanged);
            collection.ForEach(f => f.RemoveRequested += onRemoveRequested);


            collection.TypedCollectionChanged += (sender, e) =>
            {
                e.NewItems?.ForEach(f =>
                {
                    _settings.FileSettings.Add(new FileSettings { FullName = f.FullName, Id = f.Id, IsBinary = f.IsBinary, IsString = f.IsString });

                    f.PropertyChanged += onPropertyChanged;
                    f.RemoveRequested += onRemoveRequested;

                    SettingsManager.Save(_settings);
                });

                e.OldItems?.ForEach(f =>
                {
                    _settings.FileSettings.Remove(_settings.FileSettings.Where(s => s.Id == f.Id).FirstOrDefault());

                    f.PropertyChanged -= onPropertyChanged;
                    f.RemoveRequested -= onRemoveRequested;

                    SettingsManager.Save(_settings);
                });
            };


            return collection;
        }

        /// <summary></summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bAddFile_Click(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog { Multiselect = true };

            if (openFileDialog.ShowDialog() == true)
            {
                AddFiles(openFileDialog.FileNames);
            }
        }

        /// <summary></summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bConfirmClear_Click(object sender, RoutedEventArgs e)
        {
            _filesCollection.ToList().ForEach(f => _filesCollection.Remove(f));

            bShowConfirmClear.IsChecked = false;
        }

        /// <summary></summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bCancelClear_Click(object sender, RoutedEventArgs e) => bShowConfirmClear.IsChecked = false;

        /// <summary></summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bConfirmRemove_Click(object sender, RoutedEventArgs e)
        {
            _filesCollection.Where(f => f.IsSelected).ToList().ForEach(f => _filesCollection.Remove(f));

            bShowConfirmRemove.IsChecked = false;
        }

        /// <summary></summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bCancelRemove_Click(object sender, RoutedEventArgs e) => bShowConfirmRemove.IsChecked = false;

        /// <summary></summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbFiles_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                AddFiles((string[])e.Data.GetData(DataFormats.FileDrop));
            }
        }

        /// <summary></summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbFiles_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (FilesTreeNode file in e.AddedItems)
            {
                file.IsSelected = true;
            }

            foreach (FilesTreeNode file in e.RemovedItems)
            {
                file.IsSelected = false;
            }
        }

        /// <summary></summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbFiles_MouseDown(object sender, MouseButtonEventArgs e) => lbFiles.SelectedItem = null;

        /// <summary></summary>
        /// <param name="files"></param>
        private void AddFiles(string[] files)
        {
            foreach (var file in files)
            {
                var isString = IsFileContainsOnlyStrings(file);

                _filesCollection.Add(new FilesTreeNode(Guid.NewGuid().ToString()) { FullName = file, IsString = isString, IsBinary = !isString });
            }
        }

        /// <summary></summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private bool IsFileContainsOnlyStrings(string file)
        {
            var content = File.ReadAllText(file, Encoding.GetEncoding(1252));

            foreach (var character in content)
            {
                if (character < 0x20 || character == 0x7F)
                {
                    if (character != '\r' && character != '\n' && character != '\t') return false;
                }
            }

            return true;
        }

        /// <summary></summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bRunConversion_Click(object sender, RoutedEventArgs e)
        {
            foreach (var file in _filesCollection)
            {
                if (file.Minify && file.SaveMinified)
                {

                }

                if (file.CreateArray) tbArrays.Text += ArrayCreators.CreateArray(file, _settings);
            }
        }
    }
}