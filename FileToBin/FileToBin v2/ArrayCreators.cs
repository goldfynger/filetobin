﻿using System;
using System.IO;
using System.Text;

namespace FileToBin
{
    /// <summary></summary>
    internal static class ArrayCreators
    {
        /// <summary></summary>
        /// <param name="file"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        public static string CreateArray(FilesTreeNode file, Settings settings)
        {
            if (file.IsBinary) return CreateBinaryArray(file, settings);

            else if (file.IsString) return CreateStringArray(file, settings);

            else return null;
        }


        /// <summary></summary>
        /// <param name="file"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        private static string CreateBinaryArray(FilesTreeNode file, Settings settings)
        {
            var content = file.Minify ? file.GetBinaryMinifyContent() : file.GetBinaryContent();

            var length = int.Parse(settings.BinarySettings.LengthCollection[settings.BinarySettings.LengthIndex]);
            var tabLength = int.Parse(settings.BinarySettings.TabLengthCollection[settings.BinarySettings.TabLengthIndex]);
            var addLength = settings.BinarySettings.AddArrayLength;
            var addInfo = settings.BinarySettings.AddArrayInfo;

            var result = new StringBuilder();

            var fileName = Path.GetFileName(file.FullName);

            result.Append($"{Environment.NewLine}const uint8_t _{fileName.Split('.')[0]}[{(addLength ? (content.Length).ToString() : string.Empty)}] =");

            if (addInfo)
            {
                result.Append($" /* {content.Length} {fileName} */");
            }

            result.Append($"{Environment.NewLine}{{{Environment.NewLine}");

            var strCount = content.Length / length;

            if ((content.Length % length) != 0) strCount++;

            var counter = 0;

            for (var i = 0; i < strCount; i++)
            {
                for (var j = 0; j < tabLength; j++) result.Append(' ');

                for (var j = 0; j < length; j++)
                {
                    if (counter == content.Length) break;

                    result.Append($"0x{content[counter++].ToString("X2")}");

                    if (j != length - 1 && counter != content.Length) result.Append(", ");
                }

                if (i != strCount - 1) result.Append($",{Environment.NewLine}");
            }

            result.Append($"{Environment.NewLine}}};{Environment.NewLine}");

            return result.ToString();
        }

        /// <summary></summary>
        /// <param name="file"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        private static string CreateStringArray(FilesTreeNode file, Settings settings)
        {
            var content = file.Minify ? file.GetStringMinifyContent() : file.GetStringContent();

            var length = int.Parse(settings.StringSettings.LengthCollection[settings.StringSettings.LengthIndex]);
            var tabLength = int.Parse(settings.StringSettings.TabLengthCollection[settings.StringSettings.TabLengthIndex]);
            var addLength = settings.StringSettings.AddArrayLength;
            var addInfo = settings.StringSettings.AddArrayInfo;

            var zeroSymbols = false;
            var unexpectedSymbols = false;

            var raw = new StringBuilder(content);
            var rawCounter = 0;
            while (rawCounter < raw.Length)
            {
                if (raw[rawCounter] == '"') raw.Insert(rawCounter++, '\\');
                else if (raw[rawCounter] == '\r')
                {
                    raw[rawCounter] = 'r';
                    raw.Insert(rawCounter++, '\\');
                }
                else if (raw[rawCounter] == '\n')
                {
                    raw[rawCounter] = 'n';
                    raw.Insert(rawCounter++, '\\');
                }
                else if (raw[rawCounter] == '\t')
                {
                    raw[rawCounter] = 't';
                    raw.Insert(rawCounter++, '\\');
                }
                else if (raw[rawCounter] == '\0')
                {
                    raw[rawCounter] = '0';
                    raw.Insert(rawCounter++, '\\');

                    zeroSymbols = true;
                }
                else if (raw[rawCounter] == '\\')
                {
                    raw[rawCounter] = '\\';
                    raw.Insert(rawCounter++, '\\');
                }
                else if (raw[rawCounter] < 0x20 || raw[rawCounter] == 0x7F)
                {
                    unexpectedSymbols = true;
                }

                rawCounter++;
            }

            var result = new StringBuilder();

            var fileName = Path.GetFileName(file.FullName);

            var warningStr = !zeroSymbols && !unexpectedSymbols ? string.Empty :
                $"Warning:{(zeroSymbols ? " Zero symbol(s) in file;" : string.Empty)}{(unexpectedSymbols ? " Unexpected character(s) in file;" : string.Empty)}";

            result.Append($"{Environment.NewLine}const char _{fileName.Split('.')[0]}[{(addLength ? (content.Length + 1).ToString() : string.Empty)}] =");

            if (addInfo || !string.IsNullOrWhiteSpace(warningStr))
            {
                result.Append($" /* {content.Length}+1 {fileName} {warningStr} */");
            }

            result.Append($"{Environment.NewLine}{{{Environment.NewLine}");

            var rawStr = raw.ToString();

            var counter = 0;

            while (true)
            {
                for (var j = 0; j < tabLength; j++) result.Append(' ');

                var len = (counter + length > rawStr.Length - 1) ? rawStr.Length - counter : length;

                if (rawStr[counter + len - 1] == '\\' && counter + len != rawStr.Length) len++;

                result.Append($"\"{rawStr.Substring(counter, len)}\"");

                counter += len;

                if (rawStr.Length != counter) result.Append($"{Environment.NewLine}");
                else break;
            }

            result.Append($"{Environment.NewLine}}};{Environment.NewLine}");

            return result.ToString();
        }
    }
}